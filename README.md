# blockgap

Finds periods w/ scarse bitcoin block production

The program currently:
- Starts evaluating at block 391000 (start of 2016).
- Uses the median blocktime of the last three blocks.
- Counts blocks in the last 2 hours.
- Reports recent block counts of 6 or less.


## Running

```
export RPCUSER=rpcuser
export RPCPASSWORD=blahblahblah

./blockgap.py
```

